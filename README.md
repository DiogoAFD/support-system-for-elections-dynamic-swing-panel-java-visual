# **Support System for Elections** #
###  ###
The system has two operating modes:

* Configuration of an election (define type of election, candidates, voters, etc.);
* Manage the electoral process itself (register votes, assign mandates, declare the winner, etc.).

These two modes are available in one application.