/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.EleicoesDAO;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Jorge
 */
public class EleicoesFacade {

    protected EleicoesDAO eleicoes;

    public EleicoesFacade() {
        this.eleicoes = new EleicoesDAO();
    }

    public List<Object[]> listaEleicoes() {

        ArrayList<Eleicao> aux = new ArrayList<>(this.eleicoes.values());
        ArrayList<Object[]> o = new ArrayList<>();

        for (Eleicao e : aux) {
            o.add(new Object[]{e.getDesignacao(), e.getDataFim().toString().replace('T', ' '), e.getId()});
        }

        return o;
    }

    public Object[] listaEleicao(int id) {

        Eleicao e = this.eleicoes.get(id);

        Object[] res = {e.getDesignacao(), e.getTipo(), e.getDataInicio(), e.getDataFim()};

        return res;
    }

    public List<Eleicao> filterEleicoes(int circulo, List<Eleicao> aFiltrar) {
        Iterator<Eleicao> iter = aFiltrar.iterator();
        LocalDateTime agora = LocalDateTime.now();
        while (iter.hasNext()) {
            Eleicao e = iter.next();
            if (agora.isAfter(e.getDataFim()) || agora.isBefore(e.getDataInicio())) {
                iter.remove();
            } else {
                boolean encontrado = false;
                for (Circulo c : e.getCirculos()) {
                    if (c.getId() == circulo) {
                        encontrado = true;
                    }

                }
                if (!encontrado) {
                    iter.remove();
                }
            }
        }
        return aFiltrar;
    }

    public void removeEleicao(int id) {
        this.eleicoes.remove(id);
    }

    public List<Object[]> listaEleicoesFechadas() {
        ArrayList<Eleicao> aux = new ArrayList<>(this.eleicoes.values());
        ArrayList<Object[]> o = new ArrayList<>();

        for (Eleicao e : aux) {
            if (e.isEstaFechada()) {
                o.add(new Object[]{e.getDesignacao(), e.getDataFim().toString().replace('T', ' '), e.getId()});
            }
        }

        return o;
    }

    public boolean calculaResultados(int ideleicao) {
        Eleicao e = this.eleicoes.get(ideleicao);
        if(e.isEstaFechada()){
            return false;
        }
        LocalDateTime agora = LocalDateTime.now();
        if (e.getDataFim().isAfter(agora)) {
            return false;
        }
        boolean fechar = true;
        if (e.getTipo() == 1) {
            fechar = e.setEleitos();
        }
        return fechar;
    }

    public boolean fechar(int idEleicao) {
        return eleicoes.fechar(idEleicao);
    }

    public boolean ePresidencial(int cod_elei) {
        Eleicao e = this.eleicoes.get(cod_elei);
        return e.getTipo() == 2;
    }
}
