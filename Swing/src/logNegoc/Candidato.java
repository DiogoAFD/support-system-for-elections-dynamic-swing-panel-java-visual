/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author Asus
 */
public class Candidato {

    private int id;
    private String nome;
    private boolean eleito;
    private int posicao;
    private Circulo cir;

    public Candidato(String nome, Circulo cir) {
        this.nome = nome;
        this.cir = cir;
    }

    public Candidato(String nome, Circulo cir, int pos) {
        this.nome = nome;
        this.cir = cir;
        this.posicao = pos;
    }

    public Candidato(int id, String nome, Circulo cir, int pos) {
        this.id = id;
        this.nome = nome;
        this.cir = cir;
        this.posicao = pos;
        this.eleito = false;
    }

    public Candidato(int id, String nome, boolean eleito, int posicao, Circulo cir) {
        this.id = id;
        this.nome = nome;
        this.eleito = eleito;
        this.posicao = posicao;
        this.cir = cir;
    }

    public int getId() {
        return id;
    }

    public int getPosicao() {
        return posicao;
    }

    public boolean isEleito() {
        return eleito;
    }

    public Circulo getCir() {
        return cir;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEleito(boolean eleito) {
        this.eleito = eleito;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        return hash;
    }

}
