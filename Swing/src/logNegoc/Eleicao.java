/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.CandidaturasDAO;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 *
 * @author rcamposinhos
 */
public class Eleicao {

    private int id;
    private String designacao;
    private LocalDateTime dataInicio;
    private LocalDateTime dataFim;
    private CandidaturasDAO candidaturas;
    private ArrayList<Circulo> circulos;
    private boolean estaFechada;
    private int tipo;

    public Eleicao(String designacao, LocalDateTime dataInicio, LocalDateTime dataFim, ArrayList<Circulo> circulos, int tipo) {
        this.designacao = designacao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.candidaturas = new CandidaturasDAO();
        this.circulos = circulos;
        this.tipo = tipo;
        this.estaFechada = false;
    }

    public Eleicao(int id) {
        this.id = id;
    }

    public Eleicao(int id, String designacao, LocalDateTime dataInicio, LocalDateTime dataFim, ArrayList<Circulo> circulos, boolean estaFechada, int tipo) {
        this.id = id;
        this.designacao = designacao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.candidaturas = new CandidaturasDAO();
        this.circulos = circulos;
        this.estaFechada = estaFechada;
        this.tipo = tipo;
    }

    public boolean isEstaFechada() {
        return estaFechada;
    }

    public int getId() {
        return id;
    }

    public String getDesignacao() {
        return designacao;
    }

    public LocalDateTime getDataInicio() {
        return dataInicio;
    }

    public LocalDateTime getDataFim() {
        return dataFim;
    }

    public ArrayList<Circulo> getCirculos() {
        return circulos;
    }

    public int getTipo() {
        return tipo;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        Eleicao a = (Eleicao) o;
        return this.id == a.getId();
    }

    public Map<Candidatura, Integer> resultadosGlobais() {
        Map<Candidatura, Integer> res = new HashMap<>();
        //acumular resultados em todas as candidaturas
        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            int resCand = 0;
            for (Votos v : cand.getVotosByFreg()) {
                resCand += v.getNumVotos();
            }
            res.put(cand, resCand);
        }

        return res;
    }

    private ArrayList<Candidatura> getCandByCirc(Circulo c) {
        ArrayList<Candidatura> cands = new ArrayList<>(this.candidaturas.valuesByElei(id));
        Iterator<Candidatura> iter = cands.iterator();
        while(iter.hasNext()){
            Candidatura can=iter.next();
            boolean existe = false;
            for (Candidato aux : can.getListas()) {
                if (aux.getCir().getId() == c.getId()) {
                    existe = true;
                }
                if (existe) {
                    break;
                }
            }
            if (!existe) {
                iter.remove();
            }

        }

        return cands;
    }

    //para fechar eleicao --- calculo de mandatos
    private Map<Candidatura, Integer> atribuirMandatos(Circulo c) {
        Map<Candidatura, Integer> mandatos = new HashMap<>();
        Map<Candidatura, Integer> votos = new HashMap<>();
        int mandDisponiveis = c.getMandatos();
        Collection<Candidatura> cands = getCandByCirc(c);

        //calcular votos por candidatura
        for (Candidatura cand : cands) {
            votos.put(cand, cand.calculaVotosCirculo(c));
        }
        

        //votos por Hondt --- insercao ordenada
        TreeSet<ParResultado> resHondt = new TreeSet<>(new ComparatorParResultado());
        for (Map.Entry<Candidatura, Integer> entry : votos.entrySet()) {
            //Skip para votos em branco
            if (!(entry.getKey().getDesignacao().equals("BRANCO"))) {
                for (int i = 1; i <= mandDisponiveis; i++) {
                    ParResultado par = new ParResultado(entry.getKey(), entry.getValue() / i);
                    resHondt.add(par);
                }
            }
        }
        
        //inicializar mandatos
        for (Candidatura cand : cands) {
            mandatos.put(cand, 0);
        }

        //atribuicao ordenada
        int i = 1;
        for (ParResultado aux : resHondt) {
            if (i > mandDisponiveis) {
                break;
            }
            Candidatura candAux = aux.getCandidatura();
            int nMandatos = (mandatos.get(candAux)) + 1;
            mandatos.put(candAux, nMandatos);
            i++;
        }
        
        

        return mandatos;
    }

    //so para fechar eleicao --- calculo de mandatos
    //registar os eleitos de uma lista na BD
    public boolean setEleitos() {
        boolean res = true;
        for (Circulo c : this.circulos) {
            Map<Candidatura, Integer> mandatos = atribuirMandatos(c);
            for (Map.Entry<Candidatura, Integer> entry : mandatos.entrySet()) {
                res = entry.getKey().setEleitos(entry.getValue(), c);
                if (res == false) {
                    break;
                }
            }
            if (res == false) {
                break;
            }
        }

        return res;
    }

    public int totalVotos() {
        int res = 0;

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            for (Votos v : cand.getVotosByFreg()) {
                res += v.getNumVotos();
            }
        }

        return res;
    }

    public List<Object[]> resultados() {
        ArrayList<Object[]> res = new ArrayList<>();

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            int totVotos = cand.calculaVotosGlobais();
            if (this.tipo == 1) {
                int totMand = cand.totMandatos();
                res.add(new Object[]{cand.getDesignacao(), totVotos, totMand});
            } else {
                res.add(new Object[]{cand.getDesignacao(), totVotos, "-"});
            }
        }
        return res;
    }

    public int totalVotosbyCirculo(Circulo codCirc) {
        int res = 0;

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            res += cand.calculaVotosCirculo(codCirc);
        }

        return res;
    }

    public List<Object[]> resultadosByCirculo(Circulo codCir) {
        ArrayList<Object[]> res = new ArrayList<>();

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            int totVotos = cand.calculaVotosCirculo(codCir);
            if (tipo == 1) {
                int totMand = cand.totMandatosByCirc(codCir.getId());
                res.add(new Object[]{cand.getDesignacao(), totVotos, totMand});
            } else {
                res.add(new Object[]{cand.getDesignacao(), totVotos, "-"});
            }
        }
        return res;
    }

    public int totalVotosbyConcelho(Concelho codCon) {
        int res = 0;

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            res += cand.calculaVotosConcelho(codCon);
        }

        return res;
    }

    public List<Object[]> resultadosByConcelho(Concelho con) {
        ArrayList<Object[]> res = new ArrayList<>();

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            int totVotos = cand.calculaVotosConcelho(con);
            res.add(new Object[]{cand.getDesignacao(), totVotos, "-"});
        }
        return res;
    }

    int totalVotosbyFreg(Freguesia freg) {
        int res = 0;

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            res += cand.calculaVotosFreguesia(freg);
        }

        return res;
    }

    List<Object[]> resultadosByFreguesia(Freguesia freg) {
        ArrayList<Object[]> res = new ArrayList<>();

        for (Candidatura cand : this.candidaturas.valuesByElei(id)) {
            int totVotos = cand.calculaVotosFreguesia(freg);
            res.add(new Object[]{cand.getDesignacao(), totVotos, "-"});
        }
        return res;
    }

}
