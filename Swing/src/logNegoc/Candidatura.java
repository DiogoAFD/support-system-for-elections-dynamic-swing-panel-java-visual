/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.CandidaturasDAO;
import data.ConcelhosDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author rcamposinhos
 */
public class Candidatura {

    private int id;
    private String designacao;
    private String logotipoFilePath;
    private Eleicao elec;
    private ArrayList<Candidato> listas;
    private ArrayList<Votos> votosByFreg;

    public Candidatura(String designacao, String logotipoFilePath, Eleicao elec, ArrayList<Candidato> listas, ArrayList<Votos> votosByFreg) {
        this.designacao = designacao;
        this.logotipoFilePath = logotipoFilePath;
        this.elec = elec;
        this.listas = listas;
        this.votosByFreg = votosByFreg;
    }

    public Candidatura(String designacao, String logotipoFilePath, Eleicao elec, ArrayList<Candidato> listas) {
        this.designacao = designacao;
        this.logotipoFilePath = logotipoFilePath;
        this.elec = elec;
        this.listas = listas;
    }

    public Candidatura(int id, String designacao, String logotipoFilePath, Eleicao elec, ArrayList<Candidato> listas, ArrayList<Votos> votosByFreg) {
        this.id = id;
        this.designacao = designacao;
        this.logotipoFilePath = logotipoFilePath;
        this.elec = elec;
        this.listas = listas;
        this.votosByFreg = votosByFreg;
    }

    public int getId() {
        return id;
    }

    public String getDesignacao() {
        return designacao;
    }

    public String getLogotipoFilePath() {
        return logotipoFilePath;
    }

    public Eleicao getElec() {
        return elec;
    }

    public ArrayList<Candidato> getListas() {
        return listas;
    }

    public ArrayList<Votos> getVotosByFreg() {
        return votosByFreg;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    public String getHTML() {
        return "<html><body><p><img src=" + this.logotipoFilePath + " width=\"30\" height=\"30\">&nbsp;" + this.designacao + "</p></body></html>";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.id;
        return hash;
    }

    public Set<Circulo> getCirculos() {
        Set<Circulo> res = new HashSet<>();
        for (Candidato c : this.listas) {
            res.add(c.getCir());
        }

        return res;
    }

    //resultados ordenados por posicao
    public Set<Candidato> getCandidatosByCirc(Circulo circ) {
        Set<Candidato> res = new TreeSet<>(new ComparatorCandidato());
        for (Candidato c : this.listas) {
            if (c.getCir().getId() == circ.getId()) {
                res.add(c);
            }
        }

        return res;
    }

    public Set<Concelho> getConcelhos() {
        Set<Concelho> res = new HashSet<>();
        ConcelhosDAO c = new ConcelhosDAO();
        for (Votos v : this.votosByFreg) {
            int conId = v.getFreguesia().getConcelho().getId();
            res.add(c.get(conId));
        }

        return res;
    }

    public int calculaVotosCirculo(Circulo c) {
        int res = 0;

        for (Votos v : this.votosByFreg) {
            int codCirc = v.getFreguesia().getConcelho().getCirculo().getId();
            if (codCirc == c.getId()) {
                res += v.getNumVotos();
            }
        }

        return res;
    }

    public int calculaVotosConcelho(Concelho c) {
        int res = 0;

        for (Votos v : this.votosByFreg) {
            int codConc = v.getFreguesia().getConcelho().getId();
            if (codConc == c.getId()) {
                res += v.getNumVotos();
            }
        }

        return res;
    }

    public int calculaVotosFreguesia(Freguesia c) {
        int res = 0;

        for (Votos v : this.votosByFreg) {
            if (v.getFreguesia().getId() == c.getId()) {
                res += v.getNumVotos();
                break;
            }
        }

        return res;
    }

    public int calculaVotosGlobais() {
        int res = 0;

        for (Votos v : this.votosByFreg) {
            res += v.getNumVotos();
        }

        return res;
    }

    public Map<Circulo, Integer> resultadosByCirculo() {
        Map<Circulo, Integer> res = new HashMap<>();

        Set<Circulo> circulos = this.getCirculos();

        for (Circulo c : circulos) {
            res.put(c, calculaVotosCirculo(c));
        }

        return res;
    }

    public Map<Concelho, Integer> resultadosByConcelho() {

        Map<Concelho, Integer> res = new HashMap<>();

        Set<Concelho> concelhos = this.getConcelhos();

        for (Concelho c : concelhos) {
            res.put(c, calculaVotosConcelho(c));
        }

        return res;

    }

    //return true if success
    public boolean setEleitos(int nMandatos, Circulo c) {
        Set<Candidato> candidatos = getCandidatosByCirc(c);
        CandidaturasDAO aux = new CandidaturasDAO();
        Iterator<Candidato> it = candidatos.iterator();
        int i = 0;
        while (it.hasNext() && i < nMandatos) {
            it.next().setEleito(true);
            i++;
        }
        return aux.setEleitos(candidatos);
    }

    public int totMandatos() {
        int res = 0;
        for (Candidato c : listas) {
            if (c.isEleito()) {
                res++;
            }
        }
        return res;
    }

    public int totMandatosByCirc(int codCir) {
        int res = 0;
        for (Candidato c : listas) {
            if (c.isEleito() && c.getCir().getId() == codCir) {
                res++;
            }
        }
        return res;
    }

}
