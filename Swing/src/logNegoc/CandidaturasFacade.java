/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.CandidaturasDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Jorge
 */
public class CandidaturasFacade {

    protected CandidaturasDAO candidaturas;

    public CandidaturasFacade() {
        this.candidaturas = new CandidaturasDAO();
    }

    public List<Object[]> listaCandidaturas() {
        ArrayList<Candidatura> aux = new ArrayList<>(this.candidaturas.values());
        ArrayList<Object[]> o = new ArrayList<>();

        for (Candidatura e : aux) {
            o.add(new Object[]{e.getDesignacao(), e.getElec().getDesignacao()});
        }

        return o;
    }

    public List<String[]> getCandByElei(int elei, int codCir) {
        ArrayList<Candidatura> aux = new ArrayList<>(this.candidaturas.valuesByElei(elei));
        List<String[]> o = new ArrayList<>();

        int i = 0;
        for (Candidatura c : aux) {
            if (c.getDesignacao().equals("BRANCO")) {
                String[] aux2 = new String[2];
                aux2[0] = Integer.toString(c.getId());
                aux2[1] = c.getHTML();
                o.add(aux2);
            }
            for (Candidato can : c.getListas()) {
                if (can.getCir().getId() == codCir) {
                    String[] aux2 = new String[2];
                    aux2[0] = Integer.toString(c.getId());
                    aux2[1] = c.getHTML();
                    o.add(aux2);
                    break;
                }
            }
        }

        return o;
    }

    public List<String[]> getCandByElei(int elei) {
        ArrayList<Candidatura> aux = new ArrayList<>(this.candidaturas.valuesByElei(elei));
        List<String[]> o = new ArrayList<>();

        for (Candidatura c : aux) {
            String[] aux2 = new String[2];
            aux2[0] = Integer.toString(c.getId());
            aux2[1] = c.getHTML();
            o.add(aux2);

        }

        return o;
    }

    public HashMap<String, Integer> getCirculosFromCandidatura(int cod_cand) {
        HashMap<String, Integer> res = new HashMap<>();
        Candidatura c = candidaturas.get(cod_cand);
        for (Candidato ci : c.getListas()) {
            res.put(ci.getCir().getDesignacao(), ci.getCir().getId());
        }
        return res;
    }

    public ArrayList<String> getCandidatosByCandidaturaByCirculo(int cod_candidatura, int circuloId) {
        ArrayList<String> res = new ArrayList<>();
        Candidatura c = this.candidaturas.get(cod_candidatura);
        for (Candidato can : c.getListas()) {
            if (can.getCir().getId() == circuloId) {
                if (can.isEleito()) {
                    res.add(can.getNome() + " -- Eleito");
                }
            }
        }
        for (Candidato can : c.getListas()) {
            if (can.getCir().getId() == circuloId) {
                if (!can.isEleito()) {
                    res.add(can.getNome());
                }
            }
        }
        return res;
    }
}
