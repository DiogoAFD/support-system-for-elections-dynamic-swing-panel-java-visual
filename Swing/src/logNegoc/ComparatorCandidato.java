/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import java.util.Comparator;

/**
 *
 * @author rcamposinhos
 */
public class ComparatorCandidato implements Comparator<Candidato> {

    @Override
    public int compare(Candidato o1, Candidato o2) {
        return o1.getPosicao() - o2.getPosicao();
    }

}
