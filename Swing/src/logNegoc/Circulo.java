/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author rcamposinhos
 */
public class Circulo {

    private int id;
    private String designacao;
    private int mandatos;

    public Circulo() {
    }

    public Circulo(String designacao) {
        this.designacao = designacao;
    }

    public Circulo(int id, String designacao) {
        this.id = id;
        this.designacao = designacao;
        this.mandatos = 0;
    }

    public Circulo(int id, String designacao, int mandatos) {
        this.id = id;
        this.designacao = designacao;
        this.mandatos = mandatos;
    }

    public String getDesignacao() {
        return designacao;
    }

    public int getId() {
        return id;
    }

    public int getMandatos() {
        return this.mandatos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

}
