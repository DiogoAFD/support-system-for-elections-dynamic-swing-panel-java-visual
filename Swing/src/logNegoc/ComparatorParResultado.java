/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import java.util.Comparator;

/**
 *
 * @author rcamposinhos
 */
public class ComparatorParResultado implements Comparator<ParResultado> {

    @Override
    public int compare(ParResultado o1, ParResultado o2) {
        int diferenca = o2.getVotosHondt() - o1.getVotosHondt();
        if (diferenca == 0) {
            return 1;
        } else {
            return diferenca;
        }
    }

}
