/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

/**
 *
 * @author Asus
 */
public class Votos {
    
    private int numVotos;
    private Freguesia freguesia;

    public Votos(int numVotos, Freguesia freguesia) {
        this.numVotos = numVotos;
        this.freguesia = freguesia;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getNumVotos() {
        return numVotos;
    }

    public Freguesia getFreguesia() {
        return freguesia;
    }
    
    
    
}
