/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logNegoc;

import data.CirculosDAO;
import data.ConcelhosDAO;
import data.FreguesiasDAO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Asus
 */
public class LocaisFacade {
    
    protected FreguesiasDAO freguesias;
    protected ConcelhosDAO concelhos;
    protected CirculosDAO circulos;

    public LocaisFacade() {
        this.concelhos = new ConcelhosDAO();
        this.circulos = new CirculosDAO();
        this.freguesias = new FreguesiasDAO();
    }
    
    //Freguesias
    
    public void criarFreguesia(String designacaoFreg, int idConcelho) {
        Concelho conc = concelhos.get(idConcelho);
        Freguesia freg = new Freguesia(designacaoFreg, conc);
        freguesias.put(1, freg);
    }

    public void altFreguesia(int idFregAntiga, String nomeNovo) {
        freguesias.update(idFregAntiga, nomeNovo);
    }

    public void remFreguesia(int idFreguesia) {
        freguesias.remove(idFreguesia);
    }

    public HashMap<String, Integer> getAllFregNameId() {
        HashMap<String, Integer> res = new HashMap<>();
        Collection<Freguesia> freguesiasAux = freguesias.values();
        for (Freguesia f : freguesiasAux) {
            res.put(f.getDesignacao(), f.getId());
        }
        return res;
    }

    public ArrayList<Freguesia> getFreguesias() {
        ArrayList<Freguesia> aux = new ArrayList<>(this.freguesias.values());
        return aux;
    }

    public String[] getFreguesiasByConcelho(int idConcelho) {
        Concelho c = concelhos.get(idConcelho);
        ArrayList<Freguesia> aux = new ArrayList<>(this.freguesias.values());
        String[] res = new String[aux.size()];
        int index = 0;
        for (Freguesia f : aux) {
            if (f.getConcelho().getId() == c.getId()) {
                res[index] = f.getDesignacao();
                index++;
            }
        }
        String[] res2 = new String[index];
        System.arraycopy(res, 0, res2, 0, index);
        return res2;
    }
    
    public Collection<Freguesia> getFreguesiasByConcelho2(int idConcelho) {
        Concelho c = concelhos.get(idConcelho);
        ArrayList<Freguesia> aux = new ArrayList<>(this.freguesias.values());
        Collection<Freguesia> res=new ArrayList<>();
        for (Freguesia f : aux) {
            if (f.getConcelho().getId() == c.getId()) {
                res.add(f);
            }
        }
        return res;
    }

    public boolean FregHasConc(int idConcelho, String nomeFreguesia) {

        ArrayList<Freguesia> freguesiasAux = this.getFreguesias();

        for (Freguesia f : freguesiasAux) {
            if (f.getConcelho().getId() == idConcelho) {
                if (f.getDesignacao().equals(nomeFreguesia)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    //Concelho
    
    public void criarConcelho(String designacaoConc, int idCirculo) {
        Circulo circ = circulos.get(idCirculo);
        Concelho conc = new Concelho(designacaoConc, circ);
        concelhos.put(1, conc);
    }

    public void altConcelho(int idConcAntigo, String nomeNovo) {
        concelhos.update(idConcAntigo, nomeNovo);
    }

    public void remConcelho(Integer idConcelho) {
        concelhos.remove(idConcelho);
    }

    public HashMap<String, Integer> getAllConcNameId() {
        HashMap<String, Integer> res = new HashMap<>();
        Collection<Concelho> concelhosAux = concelhos.values();
        for (Concelho c : concelhosAux) {
            res.put(c.getDesignacao(), c.getId());
        }
        return res;
    }

    public String[] getConcelhosByCirculo(int idCirculo) {
        Circulo circ = circulos.get(idCirculo);
        ArrayList<Concelho> aux = new ArrayList<>(this.concelhos.values());
        String[] res = new String[aux.size()];
        //res[0]="";
        int index = 0;
        for (Concelho c : aux) {
            if (c.getCirculo().getId() == circ.getId()) {
                res[index] = c.getDesignacao();
                index++;
            }
        }
        String[] res2 = new String[index];
        System.arraycopy(res, 0, res2, 0, index);
        return res2;
    }
    
    //Circulo
    
    public void criarCirculo(String designacao) {
        Circulo c = new Circulo(designacao);
        circulos.put(1, c);
    }

    public void remCirculo(Integer idCirculo) {
        circulos.remove(idCirculo);
    }

    public void altCirculo(int idCircAntigo, String nomeNovo) {
        circulos.update(idCircAntigo, nomeNovo);
    }

    public HashMap<String, Integer> getAllCircNameId() {
        HashMap<String, Integer> res = new HashMap<>();
        Collection<Circulo> circulosAux = circulos.values();
        for (Circulo c : circulosAux) {
            res.put(c.getDesignacao(), c.getId());
        }
        return res;
    }
    
    public HashMap<String, Integer> getCircNameIdWithoutConcelhos() {
        HashMap<String, Integer> res = new HashMap<>();
        HashSet<String> aux=this.getCirculosDesComConcelhos();
        Collection<Circulo> circulosAux = circulos.values();
        for (Circulo c : circulosAux) {
            if(!aux.contains(c.getDesignacao())){
                res.put(c.getDesignacao(), c.getId());
            }
        }
        return res;
    }
    
    private HashSet<String> getCirculosDesComConcelhos() {
        HashSet<String> res=new HashSet<>();
        ArrayList<Concelho> aux = new ArrayList<>(this.concelhos.values());
        for(Concelho c:aux){
            res.add(c.getCirculo().getDesignacao());
        }
        return res;
    }
    
    public HashMap<String, Integer> getConcNameIdWithoutFreguesias() {
        HashMap<String, Integer> res = new HashMap<>();
        HashSet<String> aux=this.getConcelhosDesComFreguesias();
        Collection<Concelho> circulosAux = concelhos.values();
        for (Concelho c : circulosAux) {
            if(!aux.contains(c.getDesignacao())){
                res.put(c.getDesignacao(), c.getId());
            }
        }
        return res;
    }
    
    private HashSet<String> getConcelhosDesComFreguesias() {
        HashSet<String> res=new HashSet<>();
        ArrayList<Freguesia> aux = new ArrayList<>(this.freguesias.values());
        for(Freguesia c:aux){
            res.add(c.getConcelho().getDesignacao());
        }
        return res;
    }
    
    public String[] getCirculosName() {
        ArrayList<Circulo> aux = new ArrayList<>(this.circulos.values());
        String[] circulosAux = new String[aux.size()];

        int index = 0;
        for (Circulo c : aux) {
            circulosAux[index] = c.getDesignacao();
            index++;
        }
        return circulosAux;
    }

    public ArrayList<String[]> getCirculosArray() {
        ArrayList<Circulo> aux = new ArrayList<>(this.circulos.values());
        ArrayList<String[]> res = new ArrayList<>();

        for (Circulo c : aux) {
            String[] aux2 = new String[2];
            aux2[0] = Integer.toString(c.getId());
            aux2[1] = c.getDesignacao();
            res.add(aux2);
        }
        return res;
    }

    public ArrayList<String[]> getCirculosEleicao(int id) {

        ArrayList<String[]> aux = new ArrayList<>();

        for (Circulo c : this.circulos.getByEleicao(id)) {
            String[] s = new String[]{Integer.toString(c.getId()), c.getDesignacao(), Integer.toString(c.getMandatos())};
            aux.add(s);
        }

        return aux;
    }

    public Circulo getIdCirculoByName(String nome) {
        return this.circulos.getByName(nome);
    }

    

    
    
    
}
