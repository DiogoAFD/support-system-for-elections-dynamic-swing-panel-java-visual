/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.*;
import java.util.ArrayList;
import logNegoc.Votante;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import logNegoc.Eleicao;
import logNegoc.Eleitor;

/**
 *
 * @author rcamposinhos
 */
public class VotantesDAO implements Map<Integer, List<Votante>> {

    public VotantesDAO() {

    }

    @Override
    public int size() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsKey(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Votante> get(Object key) {
        Votante e = null;
        Connection con = null;
        EleitoresDAO eleiDAO = new EleitoresDAO();
        EleicoesDAO elecDAO = new EleicoesDAO();
        ArrayList<Votante> all_vot = new ArrayList<>();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from eleitoreleicao where eleitor= ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Eleitor e2 = eleiDAO.get(rs.getInt("eleitor"));
                Eleicao eleic = elecDAO.get(rs.getInt("eleicao"));
                e = new Votante(e2, eleic);
                all_vot.add(e);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return all_vot;
    }

    @Override
    public List<Votante> put(Integer key, List<Votante> value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Votante> remove(Object key) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends List<Votante>> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<List<Votante>> values() {
        Collection<List<Votante>> res = new ArrayList<>();
        List<Votante> c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select eleitor from eleitoreleicao");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                c = this.get(rs.getInt(1));
                res.add(c);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public Set<Entry<Integer, List<Votante>>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
