/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.*;
import java.util.ArrayList;
import logNegoc.Candidatura;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import logNegoc.Candidato;
import logNegoc.Circulo;
import logNegoc.Eleicao;
import logNegoc.Freguesia;
import logNegoc.Votos;

/**
 *
 * @author Toshiba
 */
public class CandidaturasDAO implements Map<Integer, Candidatura> {

    @Override
    public int size() {
        int size = -1;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select count(idCandidatura) from candidatura");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        int size = this.size();
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Candidatura e = this.get(Integer.parseInt(key.toString()));
        return e != null;
    }

    @Override
    public Candidatura get(Object key) {
        Candidatura e = null;
        Connection con = null;
        CirculosDAO cirDAO = new CirculosDAO();
        EleicoesDAO eleDAO = new EleicoesDAO();
        FreguesiasDAO fregDAO = new FreguesiasDAO();

        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from candidatura where idcandidatura= ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                /*Vai buscar todos os candidatos */
                ArrayList<Candidato> list_cand = new ArrayList<>();
                PreparedStatement ps2 = con.prepareStatement("select * from candidato where candidatura= ?");
                ps2.setInt(1, Integer.parseInt(key.toString()));
                ResultSet rs2 = ps2.executeQuery();

                while (rs2.next()) {
                    Circulo circ = cirDAO.get(rs2.getInt("Circulo"));
                    Candidato c = new Candidato(rs2.getInt("idCandidato"), rs2.getString("nome"), rs2.getBoolean("eleito"), rs2.getInt("posicao"), circ);
                    list_cand.add(c);
                }
                /*Vai buscar o total de votos por freguesia*/
                ArrayList<Votos> vot_by_freg = new ArrayList<>();
                PreparedStatement ps3 = con.prepareStatement("select * from candidaturaFreguesia where candidatura= ?");
                ps3.setInt(1, Integer.parseInt(key.toString()));
                ResultSet rs3 = ps3.executeQuery();

                while (rs3.next()) {
                    Freguesia f = fregDAO.get(rs3.getInt("Freguesia"));
                    int tot_vot = rs3.getInt("nrvotos");
                    Votos v = new Votos(tot_vot, f);
                    vot_by_freg.add(v);
                }
                Eleicao ele = eleDAO.get(rs.getInt("eleicao"));
                e = new Candidatura(rs.getInt("idcandidatura"), rs.getString("designacao"), rs.getString("logotipofilepath"), ele, list_cand, vot_by_freg);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return e;
    }

    @Override
    public Candidatura put(Integer key, Candidatura value) {
        return this.put(value);
    }

    public Candidatura put(Candidatura value) {
        Connection con = null;
        Set<Integer> circulos = new HashSet<>();
        List<Integer> freguesias = new ArrayList<>();
        CirculosDAO circDAO = new CirculosDAO();
        int aux = -1;
        try {
            con = Connect.connect();
            con.setAutoCommit(false);
            PreparedStatement ps = con.prepareStatement("insert into candidatura (designacao,logotipofilepath,eleicao) values(?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, value.getDesignacao());
            ps.setString(2, value.getLogotipoFilePath());
            ps.setInt(3, value.getElec().getId());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            //Vai buscar o id dessa candidatura
            int id_cand = -1;
            if (rs.next()) {
                id_cand = rs.getInt(1);
            }
            //Vai inserir todos os candidatos dessa candidatura
            for (Candidato aux3 : value.getListas()) {
                //Insere candidato
                PreparedStatement ps3 = con.prepareStatement("insert into candidato (nome,candidatura,circulo,eleito,posicao) values(?,?,?,?,?)");
                ps3.setString(1, aux3.getNome());
                ps3.setInt(2, id_cand);
                ps3.setInt(3, aux3.getCir().getId());
                ps3.setBoolean(4, aux3.isEleito());
                ps3.setInt(5, aux3.getPosicao());
                circulos.add(aux3.getCir().getId());
                ps3.executeUpdate();
            }
            if (value.getElec().getTipo() == 2) {
                for (Circulo c : value.getElec().getCirculos()) {
                    circulos.add(c.getId());
                }
            }
            for (Integer circulo : circulos) {
                freguesias.addAll(circDAO.getAllFreguesias(circulo));
            }

            for (Integer freguesia : freguesias) {
                PreparedStatement ps4 = con.prepareStatement("insert into candidaturafreguesia (candidatura,freguesia,nrvotos) values(?,?,0)");
                ps4.setInt(1, id_cand);
                ps4.setInt(2, freguesia);
                ps4.executeUpdate();
            }
            con.commit();
        } catch (SQLException | ClassNotFoundException ex) {
            try {
                con.rollback();
            } catch (SQLException ex1) {
                Logger.getLogger(CandidaturasDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public Candidatura remove(Object key) {
        Connection con = null;
        Candidatura rem = null;
        try {
            con = Connect.connect();
            rem = this.get(Integer.parseInt(key.toString()));
            PreparedStatement ps = con.prepareStatement("delete from candidatura where idCandidatura=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return rem;
    }

    @Override
    public Collection<Candidatura> values() {
        Collection<Candidatura> res = new ArrayList<>();
        Candidatura e = null;
        CirculosDAO cirDAO = new CirculosDAO();
        EleicoesDAO eleDAO = new EleicoesDAO();
        FreguesiasDAO fregDAO = new FreguesiasDAO();

        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from candidatura");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                /*Vai buscar todos os candidatos */
                ArrayList<Candidato> list_cand = new ArrayList<>();
                PreparedStatement ps2 = con.prepareStatement("select * from candidato where candidatura= ?");
                ps2.setInt(1, rs.getInt("idcandidatura"));
                ResultSet rs2 = ps2.executeQuery();

                while (rs2.next()) {
                    Circulo circ = cirDAO.get(rs2.getInt("Circulo"));
                    Candidato c = new Candidato(rs2.getInt("idCandidato"), rs2.getString("nome"), rs2.getBoolean("eleito"), rs2.getInt("posicao"), circ);
                    list_cand.add(c);
                }
                /*Vai buscar o total de votos por freguesia*/

                ArrayList<Votos> vot_by_freg = new ArrayList<>();
                PreparedStatement ps3 = con.prepareStatement("select * from candidaturaFreguesia where candidatura= ?");
                ps3.setInt(1, rs.getInt("idcandidatura"));
                ResultSet rs3 = ps3.executeQuery();

                while (rs3.next()) {
                    Freguesia f = fregDAO.get(rs3.getInt("Freguesia"));
                    int tot_vot = rs3.getInt("nrvotos");
                    Votos v = new Votos(tot_vot, f);
                    vot_by_freg.add(v);
                }
                Eleicao ele = eleDAO.get(rs.getInt("eleicao"));
                e = new Candidatura(rs.getInt("idcandidatura"), rs.getString("designacao"), rs.getString("logotipofilepath"), ele, list_cand, vot_by_freg);
                res.add(e);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    public Collection<Candidatura> valuesByElei(int elei) {
        Collection<Candidatura> res = new ArrayList<>();
        Candidatura e = null;
        CirculosDAO cirDAO = new CirculosDAO();
        EleicoesDAO eleDAO = new EleicoesDAO();
        FreguesiasDAO fregDAO = new FreguesiasDAO();

        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from candidatura where eleicao=?");
            ps.setInt(1, elei);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                ArrayList<Candidato> list_cand = new ArrayList<>();

                PreparedStatement ps2 = con.prepareStatement("select * from candidato where candidatura= ?");
                ps2.setInt(1, rs.getInt("idcandidatura"));
                ResultSet rs2 = ps2.executeQuery();
                /*Vai buscar todos os candidatos */
                while (rs2.next()) {

                    Circulo circ = cirDAO.get(rs2.getInt("Circulo"));
                    Candidato c = new Candidato(rs2.getInt("idCandidato"), rs2.getString("nome"), rs2.getBoolean("eleito"), rs2.getInt("posicao"), circ);
                    list_cand.add(c);
                }
                /*Vai buscar o total de votos por freguesia*/
                ArrayList<Votos> vot_by_freg = new ArrayList<>();
                PreparedStatement ps3 = con.prepareStatement("select * from candidaturaFreguesia where candidatura= ?");
                ps3.setInt(1, rs.getInt("idcandidatura"));
                ResultSet rs3 = ps3.executeQuery();
                while (rs3.next()) {
                    Freguesia f = fregDAO.get(rs3.getInt("Freguesia"));
                    int tot_vot = rs3.getInt("nrvotos");
                    Votos v = new Votos(tot_vot, f);
                    vot_by_freg.add(v);
                }
                Eleicao ele = eleDAO.get(rs.getInt("eleicao"));
                e = new Candidatura(rs.getInt("idcandidatura"), rs.getString("designacao"), rs.getString("logotipofilepath"), ele, list_cand, vot_by_freg);
                res.add(e);
            }

        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Candidatura> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Entry<Integer, Candidatura>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean registaVoto(int candidatura, int eleitor, int freguesia, int eleicao) {
        boolean flag = true;
        Connection con = null;
        int aux = -1;
        try {
            con = Connect.connect();
            con.setAutoCommit(false);
            PreparedStatement ps = con.prepareStatement("insert into eleitoreleicao (eleitor,eleicao) values(?,?)");
            ps.setInt(1, eleitor);
            ps.setInt(2, eleicao);
            ps.executeUpdate();
            PreparedStatement ps3 = con.prepareStatement("update candidaturafreguesia set nrvotos=nrvotos+1 where candidatura=? and freguesia=?");
            ps3.setInt(1, candidatura);
            ps3.setInt(2, freguesia);
            ps3.executeUpdate();
            con.commit();
        } catch (SQLException | ClassNotFoundException ex) {
            try {
                con.rollback();
                flag = false;
            } catch (SQLException ex1) {
                flag = false;
            }
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return flag;
    }

    public boolean setEleitos(Set<Candidato> eleitos) {
        Connection con = null;
        Set<Integer> circulos = new HashSet<>();
        List<Integer> freguesias = new ArrayList<>();
        CirculosDAO circDAO = new CirculosDAO();
        int aux = -1;
        boolean realizado = true;
        try {
            con = Connect.connect();
            con.setAutoCommit(false);
            for (Candidato c : eleitos) {
                if (c.isEleito()) {
                    PreparedStatement ps = con.prepareStatement("update candidato set eleito=true where idcandidato=?");
                    ps.setInt(1, c.getId());
                    ps.executeUpdate();
                }
            }
            con.commit();
        } catch (SQLException | ClassNotFoundException ex) {
            try {
                con.rollback();
                realizado = false;
            } catch (SQLException ex1) {
                Logger.getLogger(CandidaturasDAO.class.getName()).log(Level.SEVERE, null, ex1);
                realizado = false;
            }
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return realizado;
    }
}
