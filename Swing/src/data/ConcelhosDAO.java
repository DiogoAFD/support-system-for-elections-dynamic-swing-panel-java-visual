/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import logNegoc.Concelho;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.sql.*;
import java.util.ArrayList;
import logNegoc.Circulo;

/**
 *
 * @author Toshiba
 */
public class ConcelhosDAO implements Map<Integer, Concelho> {

    private Object circulos;

    public void update(int idconcAntigo, String concNovo) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("UPDATE Concelho SET Designacao = ? WHERE idConcelho = ?");
            ps.setString(1, concNovo);
            ps.setInt(2, idconcAntigo);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int size() {
        Connection con = null;
        int size = -1;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT COUNT(idConcelho) FROM Concelho");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Concelho c = this.get(Integer.parseInt(key.toString()));
        return c != null;
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Concelho get(Object key) {
        Concelho c = null;
        Connection con = null;
        Circulo cir = null;
        CirculosDAO circulos = new CirculosDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from concelho where idConcelho = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cir = circulos.get(rs.getInt("Circulo"));
                c = new Concelho(rs.getInt("idConcelho"), rs.getString("Designacao"), cir);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    @Override
    public Concelho put(Integer key, Concelho value) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("INSERT INTO Concelho(Designacao,Circulo) VALUES (?,?)");
            ps.setString(1, value.getDesignacao());
            ps.setInt(2, value.getCirculo().getId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public Concelho remove(Object key) {
        Connection con = null;
        Concelho c = null;
        try {
            con = Connect.connect();
            c = this.get(Integer.parseInt(key.toString()));
            PreparedStatement ps = con.prepareStatement("DELETE FROM Concelho WHERE idConcelho = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Concelho> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Concelho> values() {
        Collection<Concelho> res = new ArrayList<>();
        Concelho conc = null;
        Connection con = null;
        CirculosDAO circulos = new CirculosDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Concelho");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Circulo circ = circulos.get(rs.getInt("Circulo"));
                conc = new Concelho(rs.getInt("idConcelho"), rs.getString("Designacao"), circ);
                res.add(conc);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public Set<Entry<Integer, Concelho>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
