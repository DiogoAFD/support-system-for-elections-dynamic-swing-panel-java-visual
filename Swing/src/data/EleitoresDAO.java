/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.*;
import java.util.ArrayList;
import logNegoc.Eleitor;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import logNegoc.Freguesia;

/**
 *
 * @author Toshiba
 */
public class EleitoresDAO implements Map<Integer, Eleitor> {

    @Override
    public int size() {
        int size = -1;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select count(nr_eleitor) from eleitor");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        int size = this.size();
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Eleitor e = this.get(Integer.parseInt(key.toString()));
        return e != null;
    }

    @Override
    public Eleitor get(Object key) {
        Eleitor e = null;
        Freguesia f = null;
        Connection con = null;
        FreguesiasDAO freguesias = new FreguesiasDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from eleitor where nr_eleitor= ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                f = freguesias.get(rs.getInt("Freguesia"));
                e = new Eleitor(rs.getInt("nr_eleitor"), rs.getString("nome"), rs.getString("pass"), f, rs.getBoolean("eAdmin"));
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return e;
    }

    @Override
    public Eleitor put(Integer key, Eleitor value) {
        return this.put(value);
    }

    public Eleitor put(Eleitor value) {
        Connection con = null;
        int aux = -1;
        Eleitor novo = new Eleitor();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("insert into eleitor (nome,pass,freguesia,eadmin) values(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, value.getNome());
            ps.setString(2, value.getPassword());
            ps.setInt(3, value.getFreguesia().getId());
            ps.setBoolean(4, value.getAdmin());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                novo = new Eleitor(rs.getInt(1), value.getNome(), value.getPassword(), value.getFreguesia(), value.getAdmin());
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return novo;
    }

    @Override
    public Eleitor remove(Object key) {
        Connection con = null;
        Eleitor rem = null;
        try {
            con = Connect.connect();
            rem = this.get(Integer.parseInt(key.toString()));
            PreparedStatement ps = con.prepareStatement("delete from eleitor where nr_eleitor=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            return null;
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return rem;
    }

    @Override
    public Collection<Eleitor> values() {
        Collection<Eleitor> res = new ArrayList<>();
        Eleitor c = null;
        Freguesia f = null;
        Connection con = null;
        FreguesiasDAO freguesias = new FreguesiasDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from eleitor");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                f = freguesias.get(rs.getInt("Freguesia"));
                c = new Eleitor(rs.getInt("nr_eleitor"), rs.getString("nome"), rs.getString("pass"), f, rs.getBoolean("eAdmin"));
                res.add(c);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public boolean update(Eleitor value) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("update eleitor set nome=?,pass=?,freguesia=? where nr_eleitor=?");

            ps.setString(1, value.getNome());
            ps.setString(2, value.getPassword());
            ps.setInt(3, value.getFreguesia().getId());
            ps.setInt(4, value.getCodigo());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException ex) {
            return false;
        } finally {
            try {
                con.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public Set<Entry<Integer, Eleitor>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Eleitor> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
