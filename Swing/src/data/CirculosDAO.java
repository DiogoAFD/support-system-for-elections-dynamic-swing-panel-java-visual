/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import logNegoc.Circulo;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import logNegoc.Freguesia;

/**
 *
 * @author Toshiba
 */
public class CirculosDAO implements Map<Integer, Circulo> {

    public void update(int idCircAntigo, String circNovo) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("UPDATE Circulo SET Designacao = ? WHERE idCirculo = ?");
            ps.setString(1, circNovo);
            ps.setInt(2, idCircAntigo);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int size() {
        Connection con = null;
        int size = -1;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT COUNT(idCirculo) FROM Circulo");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Circulo c = this.get(Integer.parseInt(key.toString()));
        return c != null;
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Circulo get(Object key) {
        Circulo c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Circulo WHERE idCirculo = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Circulo(Integer.parseInt(key.toString()), rs.getString("Designacao"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public Circulo getWithMandatos(Object key, int eleicao) {
        Circulo c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Circulo WHERE idCirculo = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                PreparedStatement ps2 = con.prepareStatement("SELECT nrmandatos FROM eleicaocirculo WHERE Circulo = ? and eleicao=?");
                ps2.setInt(1, Integer.parseInt(key.toString()));
                ps2.setInt(2, eleicao);
                ResultSet rs2 = ps2.executeQuery();
                if (rs2.next()) {
                    c = new Circulo(Integer.parseInt(key.toString()), rs.getString("Designacao"), rs2.getInt("nrmandatos"));
                }

            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    @Override
    public Circulo put(Integer key, Circulo value) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("INSERT INTO Circulo(Designacao) VALUES (?)");
            ps.setString(1, value.getDesignacao());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public Circulo remove(Object key) {
        Connection con = null;
        Circulo c = null;
        try {
            con = Connect.connect();
            c = this.get(Integer.parseInt(key.toString()));
            PreparedStatement ps = con.prepareStatement("DELETE FROM Circulo WHERE idCirculo = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Circulo> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Circulo> values() {
        Collection<Circulo> res = new ArrayList<>();
        Circulo c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Circulo");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                //c = this.get(rs.getInt("idCirculo"));
                c = new Circulo(rs.getInt("idCirculo"), rs.getString("Designacao"));
                res.add(c);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public Set<Entry<Integer, Circulo>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Integer> getAllFreguesias(Object key) {
        List<Integer> res = new ArrayList<>();
        Freguesia c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT idfreguesia FROM circulo inner join concelho as c "
                    + "on idcirculo=c.circulo inner join freguesia as f "
                    + "on idconcelho=f.concelho where idcirculo=?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                res.add(rs.getInt("idFreguesia"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public Circulo getByName(String nome) {
        Circulo c = null;
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Circulo WHERE Designacao = ?");
            ps.setString(1, nome);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                c = new Circulo(rs.getInt("idCirculo"), nome);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public Collection<Circulo> getByEleicao(int id) {

        Circulo c = null;
        Connection con = null;
        Collection<Circulo> res = new ArrayList<>();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT Circulo,NrMandatos FROM eleicaocirculo WHERE eleicao = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int idCirculo = rs.getInt("Circulo");
                int mandatos = rs.getInt("NrMandatos");

                PreparedStatement ps2 = con.prepareStatement("SELECT designacao FROM Circulo WHERE idCirculo = ?");
                ps2.setInt(1, idCirculo);
                ResultSet rs2 = ps2.executeQuery();

                if (rs2.next()) {
                    c = new Circulo(idCirculo, rs2.getString("Designacao"), mandatos);
                }

                res.add(c);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}
