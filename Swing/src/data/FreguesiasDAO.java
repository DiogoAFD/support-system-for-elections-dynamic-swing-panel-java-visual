/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import logNegoc.Freguesia;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.sql.*;
import java.util.ArrayList;
import logNegoc.Concelho;

/**
 *
 * @author Toshiba
 */
public class FreguesiasDAO implements Map<Integer, Freguesia> {

    public void update(int idFregAntiga, String fregNova) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("UPDATE Freguesia SET Designacao = ? WHERE idFreguesia = ?");
            ps.setString(1, fregNova);
            ps.setInt(2, idFregAntiga);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int size() {
        Connection con = null;
        int size = -1;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT COUNT(idFreguesia) FROM Freguesia");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                size = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        Freguesia f = this.get(Integer.parseInt(key.toString()));
        return f != null;
    }

    @Override
    public boolean containsValue(Object value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Freguesia get(Object key) {
        Freguesia f = null;
        Connection con = null;
        Concelho conc = null;
        ConcelhosDAO concelhos = new ConcelhosDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("select * from freguesia where idFreguesia = ? ");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                conc = concelhos.get(rs.getInt("Concelho"));
                f = new Freguesia(rs.getInt("idFreguesia"), rs.getString("Designacao"), conc);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    @Override
    public Freguesia put(Integer key, Freguesia value) {
        Connection con = null;
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("INSERT INTO Freguesia(Designacao,Concelho) VALUES (?,?)");
            ps.setString(1, value.getDesignacao());
            ps.setInt(2, value.getConcelho().getId());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public Freguesia remove(Object key) {
        Connection con = null;
        Freguesia f = null;
        try {
            con = Connect.connect();
            f = this.get(Integer.parseInt(key.toString()));
            PreparedStatement ps = con.prepareStatement("DELETE FROM Freguesia WHERE idFreguesia = ?");
            ps.setInt(1, Integer.parseInt(key.toString()));
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return f;
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Freguesia> m) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Integer> keySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Collection<Freguesia> values() {
        Collection<Freguesia> res = new ArrayList<>();
        Freguesia f = null;
        Connection con = null;
        ConcelhosDAO concelhos = new ConcelhosDAO();
        try {
            con = Connect.connect();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Freguesia");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Concelho c = concelhos.get(rs.getInt("Concelho"));
                f = new Freguesia(rs.getInt("idFreguesia"), rs.getString("Designacao"), c);
                res.add(f);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    @Override
    public Set<Entry<Integer, Freguesia>> entrySet() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
