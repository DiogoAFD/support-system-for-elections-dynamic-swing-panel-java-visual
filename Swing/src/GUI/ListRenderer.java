/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author Jorge
 */
public class ListRenderer extends JLabel implements ListCellRenderer<String[]>{

    public ListRenderer(){
        setOpaque(true);
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, String[] value, int index, boolean isSelected, boolean cellHasFocus) {
        setText(value[1]);
        
        if(isSelected){
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else{
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        
        return this;
    }
}
