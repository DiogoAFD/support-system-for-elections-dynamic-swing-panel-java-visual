-- MySQL Workbench Forward Engineering
	
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema eleicao
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema eleicao
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eleicao` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `eleicao` ;

-- -----------------------------------------------------
-- Table `eleicao`.`Circulo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Circulo` (
  `idCirculo` INT NOT NULL AUTO_INCREMENT,
  `Designacao` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idCirculo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Concelho`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Concelho` (
  `idConcelho` INT NOT NULL AUTO_INCREMENT,
  `Designacao` VARCHAR(100) NOT NULL,
  `Circulo` INT NOT NULL,
  PRIMARY KEY (`idConcelho`),
  INDEX `fk_Concelho_Circulo1_idx` (`Circulo` ASC),
  CONSTRAINT `fk_Concelho_Circulo1`
    FOREIGN KEY (`Circulo`)
    REFERENCES `eleicao`.`Circulo` (`idCirculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Freguesia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Freguesia` (
  `idFreguesia` INT NOT NULL AUTO_INCREMENT,
  `Designacao` VARCHAR(100) NOT NULL,
  `Concelho` INT NOT NULL,
  PRIMARY KEY (`idFreguesia`),
  INDEX `fk_Freguesia_Concelho1_idx` (`Concelho` ASC),
  CONSTRAINT `fk_Freguesia_Concelho1`
    FOREIGN KEY (`Concelho`)
    REFERENCES `eleicao`.`Concelho` (`idConcelho`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Eleitor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Eleitor` (
  `Nr_eleitor` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(100) NOT NULL,
  `Pass` VARCHAR(45) NOT NULL,
  `Freguesia` INT NOT NULL,
  `eAdmin` TINYINT(1) NOT NULL,
  PRIMARY KEY (`Nr_eleitor`),
  INDEX `fk_Eleitor_Freguesia_idx` (`Freguesia` ASC),
  CONSTRAINT `fk_Eleitor_Freguesia`
    FOREIGN KEY (`Freguesia`)
    REFERENCES `eleicao`.`Freguesia` (`idFreguesia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Eleicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Eleicao` (
  `idEleicao` INT NOT NULL AUTO_INCREMENT,
  `Designacao` VARCHAR(100) NOT NULL,
  `DataInicio` DATETIME NOT NULL,
  `DataFim` DATETIME NOT NULL,
  `Tipo` INT NOT NULL,
  `estaFechada` TINYINT(1) NOT NULL,
  PRIMARY KEY (`idEleicao`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`EleitorEleicao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`EleitorEleicao` (
  `Eleitor` INT NOT NULL,
  `Eleicao` INT NOT NULL,
  PRIMARY KEY (`Eleitor`, `Eleicao`),
  INDEX `fk_Eleitor_has_Eleicao_Eleicao1_idx` (`Eleicao` ASC),
  INDEX `fk_Eleitor_has_Eleicao_Eleitor1_idx` (`Eleitor` ASC),
  CONSTRAINT `fk_Eleitor_has_Eleicao_Eleitor1`
    FOREIGN KEY (`Eleitor`)
    REFERENCES `eleicao`.`Eleitor` (`Nr_eleitor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Eleitor_has_Eleicao_Eleicao1`
    FOREIGN KEY (`Eleicao`)
    REFERENCES `eleicao`.`Eleicao` (`idEleicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Candidatura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Candidatura` (
  `idCandidatura` INT NOT NULL AUTO_INCREMENT,
  `Designacao` VARCHAR(100) NOT NULL,
  `LogotipoFilePath` VARCHAR(45) NOT NULL,
  `Eleicao` INT NOT NULL,
  PRIMARY KEY (`idCandidatura`),
  INDEX `fk_Candidatura_Eleicao1_idx` (`Eleicao` ASC),
  CONSTRAINT `fk_Candidatura_Eleicao1`
    FOREIGN KEY (`Eleicao`)
    REFERENCES `eleicao`.`Eleicao` (`idEleicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`Candidato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`Candidato` (
  `idCandidato` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(100) NOT NULL,
  `Candidatura` INT NOT NULL,
  `Circulo` INT NOT NULL,
  `Eleito` TINYINT(1) NULL,
  `Posicao` INT UNSIGNED NULL,
  PRIMARY KEY (`idCandidato`),
  INDEX `fk_Deputados_Candidatura1_idx` (`Candidatura` ASC),
  INDEX `fk_Deputados_Circulo1_idx` (`Circulo` ASC),
  CONSTRAINT `fk_Deputados_Candidatura1`
    FOREIGN KEY (`Candidatura`)
    REFERENCES `eleicao`.`Candidatura` (`idCandidatura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Deputados_Circulo1`
    FOREIGN KEY (`Circulo`)
    REFERENCES `eleicao`.`Circulo` (`idCirculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`CandidaturaFreguesia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`CandidaturaFreguesia` (
  `Candidatura` INT NOT NULL,
  `Freguesia` INT NOT NULL,
  `NrVotos` INT NOT NULL,
  PRIMARY KEY (`Candidatura`, `Freguesia`),
  INDEX `fk_Candidatura_has_Freguesia_Freguesia1_idx` (`Freguesia` ASC),
  INDEX `fk_Candidatura_has_Freguesia_Candidatura1_idx` (`Candidatura` ASC),
  CONSTRAINT `fk_Candidatura_has_Freguesia_Candidatura1`
    FOREIGN KEY (`Candidatura`)
    REFERENCES `eleicao`.`Candidatura` (`idCandidatura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Candidatura_has_Freguesia_Freguesia1`
    FOREIGN KEY (`Freguesia`)
    REFERENCES `eleicao`.`Freguesia` (`idFreguesia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eleicao`.`EleicaoCirculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `eleicao`.`EleicaoCirculo` (
  `Eleicao` INT NOT NULL,
  `Circulo` INT NOT NULL,
  `NrMandatos` INT NULL,
  PRIMARY KEY (`Eleicao`, `Circulo`),
  INDEX `fk_Eleicao_has_Circulo_Circulo1_idx` (`Circulo` ASC),
  INDEX `fk_Eleicao_has_Circulo_Eleicao1_idx` (`Eleicao` ASC),
  CONSTRAINT `fk_Eleicao_has_Circulo_Eleicao1`
    FOREIGN KEY (`Eleicao`)
    REFERENCES `eleicao`.`Eleicao` (`idEleicao`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Eleicao_has_Circulo_Circulo1`
    FOREIGN KEY (`Circulo`)
    REFERENCES `eleicao`.`Circulo` (`idCirculo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
